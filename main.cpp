#include <iostream>
#include<string>
#include<fstream>
#include<vector>
#include "MaxHeap.h"
using namespace std;

int howManyNumbers;
vector<int> nodeValue;

void writeToAFile()
{
	ofstream  writeFile;
	writeFile.open("input-q2a2.txt");
	writeFile << "8" << endl;
	writeFile << "19 14 52 44 69 59 81 3" << endl;
	writeFile.close();
}

void readFile()
{
	ifstream readFile;
	readFile.open("input-q2a2.txt");

	readFile >> howManyNumbers;
	for (int i = 1; i <= howManyNumbers; i++)
	{
		int temp;
		readFile >> temp;
		nodeValue.push_back(temp);
	}
	readFile.close();
}

void main()
{
	writeToAFile();

	readFile();

	MaxHeap maxHeapTree;

	maxHeapTree.insert(NumberNode(nodeValue[0]));
	maxHeapTree.insert(NumberNode(nodeValue[1]));
	maxHeapTree.insert(NumberNode(nodeValue[2]));
	maxHeapTree.insert(NumberNode(nodeValue[3]));
	maxHeapTree.insert(NumberNode(nodeValue[4]));
	maxHeapTree.insert(NumberNode(nodeValue[5]));
	maxHeapTree.insert(NumberNode(nodeValue[6]));
	maxHeapTree.insert(NumberNode(nodeValue[7]));

	for(int i=0;i<howManyNumbers;i++)
	maxHeapTree.heapifyDown(i);
	
	// Writing the output to file in MaxHeap.cpp at showHeap

	maxHeapTree.showHeap();
	system("pause");

}