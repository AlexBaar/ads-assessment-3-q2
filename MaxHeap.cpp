#include "MaxHeap.h"
#include <fstream>


int MaxHeap::leftChildIndex(int parent)
{
    int L = 2 * parent + 1;  // equation from Geeks4Geeks from Matt used to calc array location of a parent 
    if (L < heap.size())
        return L;
    else
        return -1; // heap structure is not big enough to find that value (no child found)
}

int MaxHeap::rightChildIndex(int parent)
{
    int R = 2 * parent + 2;
    if (R < heap.size())
        return R;
    else
        return -1;
}

int MaxHeap::parentIndex(int child)
{
    int parent = (child - 1) / 2;
    if (child == 0)  // 0 meaning the ROOT , top of the tree
        return -1;
    else
        return parent;
}

void MaxHeap::heapifyUp(int index)
{
    // if index != ROOT  &&  parent index within heap bounds (not -1)  
    //                                               &&  current node's rank is smaller than its parent's
    if (index >= 0 && parentIndex(index) >= 0 && heap[index].rank < heap[parentIndex(index)].rank)
    {
        // if so, then swap values between the new node & parent
        NumberNode temp = heap[index];
        heap[index] = heap[parentIndex(index)];
        heap[parentIndex(index)] = temp;
        // after we made that single swap, we need to continue heapifyUp towards the ROOT to check all needed changes
        heapifyUp(parentIndex(index));  // this ends when we reach the ROOT
    }
}

void MaxHeap::heapifyDown(int index)
{
    // 1. compare L / R children (their indexes)
    int childL = leftChildIndex(index);
    int childR = rightChildIndex(index);
    // 2. we chose to go down left (our choice)
    int child_Index = childL;

    // if childL is not -1 && childR is not -1 && childR rank is bigger than Left 
    if (childL >= 0 && childR >= 0 && heap[childR].rank > heap[childL].rank)
        child_Index = childR; // go down right


    //  child_Index != root , nor -1     && current node > child nodes                    
    if (child_Index > 0 && heap[index].rank < heap[child_Index].rank) // index = our current index
    {    // swap, as we want the smaller elements at the top

        NumberNode temp = heap[index];
        heap[index] = heap[child_Index];
        heap[child_Index] = temp;
        // after we made that single swap, we need to continue heapifyUp towards the ROOT to check all needed changes
        heapifyDown(child_Index); // keep swapping 
    }
}

void MaxHeap::insert(NumberNode newNode_We_Inserted)
{
    // INSERT at the leaf node at the BOTTOM
    heap.push_back(newNode_We_Inserted);
    heapifyUp(heap.size() - 1); // at the last index in our array
}


void MaxHeap::showHeap()
{
    cout << " OUR heap: " << endl;
    for (NumberNode p : heap)
        cout << "[ " << p.rank << " ] " ;
    cout << endl;
    //                  **     Write the OUTPUT - printing the contents of our 2D array (vector)      **
    ofstream  writeFile;
    writeFile.open("output-q2-a2.txt");
    for (NumberNode p : heap)
        writeFile << "[ " << p.rank << " ] ";
    writeFile.close();
}



int MaxHeap::Size()
{
    return heap.size();
}
