#pragma once
#include<string>
#include"NumberNode.h"
#include<cstdlib>
#include<vector>
#include<iterator>

using namespace std;



class MaxHeap
{
public:
	vector<NumberNode> heap;  // an array that is flexible (shrink/ grow)
	int leftChildIndex(int parent);
	int rightChildIndex(int parent);
	int parentIndex(int child);
	void heapifyUp(int index);
	void heapifyDown(int index);

	void insert(NumberNode element);
	
	// SHOW the array:
	void showHeap();
	

	int Size();
};


